apt-get update && apt-get upgrade -y
apt-get install gnupg2 iptables -y

echo "deb http://repo.pritunl.com/stable/apt bionic main" | tee /etc/apt/sources.list.d/pritunl.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A

apt-get update
apt --assume-yes install pritunl

apt-get --purge autoremove
apt-get clean
apt-get -y -q autoclean
apt-get -y -q autoremove
rm -rf /tmp/*
