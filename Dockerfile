FROM ubuntu:18.04

COPY docker-install.sh /

RUN /bin/sh /docker-install.sh

ADD pritunl.conf /etc/pritunl.conf

ADD start.sh start.sh

RUN echo

EXPOSE 80
EXPOSE 443

CMD ["/usr/bin/pritunl", "start", "-c", "/etc/pritunl.conf"]

